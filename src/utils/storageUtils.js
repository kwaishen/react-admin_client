import store from 'store'
const USER_KEY = 'user_key';
export default {

  /**
   * Save user
   */
   saveUser(user) {
     store.set(USER_KEY, user);
   },

   /**
    * Load user
    */
   getUser() {
     return store.get(USER_KEY) || {};
   },

   /**
    * Delete User
    */
   deleteUser() {
     store.clearAll();
   }

}