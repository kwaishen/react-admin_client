import React, { Component } from "react";
import { Modal, Button, message, Select, Input, Alert  } from 'antd';
import { createCategory, listCategoriesByParentId } from "../../api/category-service";

const { Option } = Select;

const formValid = ({ formErrors, ...rest }) => {
  let valid = true;
  // validate form errors being empty
  Object.values(formErrors).forEach(val => {
    val.length > 0 && (valid = false);
  });

  // validate the form was filled out
  Object.values(rest).forEach(val => {
    (val === null || val.length == 0) && (valid = false);
  });

  return valid;
}

export class CategoryDialog extends React.Component {
  state = {
    ModalText: 'Content of the modal',
    visible: false,
    confirmLoading: false,
    categories: this.props.categories,
    subCategories: this.props.subCategories,
    form: {
      parentId: this.props.parentId,
      subParentId: 0,
      categoryName: '',
      formErrors: {
        parentId: '',
        subParentId: '',
        categoryName: ''
      }
    }
    
  };

  
  loadSubCategory = async () => {
    let parentId = this.state.form.parentId;
    let {categories, subCategories} = this.state;
    if ((subCategories.length > 0 && subCategories[0].parentCid === parentId)|| parentId === 0) {
      return;
    } else /*sub === 0 and parentId > 1*/ {
      // fetch from server
      const res = await listCategoriesByParentId(parentId);
      if (res.code === 0) {
        // success
        const categories = res.data;
        this.setState({
          subCategories: categories
        }, () => console.log(this.state.form))
      } else {
        message.error('Unable to fetch categories')
      }
    }
    
  }

  resetFields = () => {
    this.setState({
      form: {
        parentId: 0,
        subParentId: 0,
        categoryName: '',
        formErrors: {
          parentId: '',
          subParentId: '',
          categoryName: ''
        }
      }
    })
  }

  initFields = () => {
    this.setState({
      categories: this.props.categories,
      subCategories: this.props.subCategories,
      form: {
        parentId: this.props.parentId,
        subParentId: this.props.subParentId,
        categoryName: '',
        formErrors: {
          parentId: '',
          subParentId: '',
          categoryName: ''
        }
      }
    })
  }

  showModal = () => {
    // initilize fields
    this.initFields();
    this.setState({
      visible: true,
    });
  };

  handleCancel = () => {
    // reset fields
    this.resetFields();
    this.setState({
      visible: false,
    });
  };

  handleOk = async() => {
    const {categoryName, parentId, subParentId} = this.state.form;
    let pParentId = subParentId === 0 ? parentId : subParentId;

    if (formValid(this.state.form)) {
      console.log(`Submitting categoryName: ${categoryName} and parantId : ${pParentId}`);

      const res = await createCategory(categoryName, pParentId);
      if (res.code === 0) {
        this.setState({
          visible: false,
        });
        message.success('Category saved successfully.')
        this.props.onSaveOk();
      } else {
        message.error('Unable to save category. Please try again.');
      }

    } else {
      // set errors
      console.error("FORM INVALID - DISPLAY ERROR MESSAGE");
    }
  };

  handleChange = (e) => {
    e.preventDefault();
    const {name, value} = e.target;
    console.log(`name is ${name} and value is ${value}`)
    let formErrors = this.state.form.formErrors;

    switch (name) {
      case 'categoryName':
        formErrors.categoryName = 
          !value || value.length < 1 ? "Category name is required": "";
        break;
      default:
        break;
    }

    this.setState({
      form: {
        ...this.state.form, // keep everything else unchanged
        [name]: value
     }
    }, () => console.log(this.state.form))
  }

  handleCategoryChange = (val) => {
    console.log(`category selected ${val}`);
    this.setState({
      form: {
        ...this.state.form, // keep everything else unchanged
        'parentId': val
     }
    }, () => {
      console.log(this.state.form);
      this.loadSubCategory();
    })
  }

  handleSubCategoryChange = (val) => {
    console.log(`sub category selected ${val}`);
    this.setState({
      form: {
        ...this.state.form, // keep everything else unchanged
        subParentId: val
     }
    }, () => {
      console.log(this.state.form);
    })
  }

  render() {
    const { visible, confirmLoading, ModalText, categories, subCategories } = this.state;
    const {parentId, subParentId} = this.state.form;
    return (
      <>
        <Button type="primary" onClick={this.showModal}>
          Create
        </Button>
        <Modal
          title="Create Category"
          visible={visible}
          onOk={this.handleOk}
          confirmLoading={confirmLoading}
          onCancel={this.handleCancel}
        >
          <div className="panel-body">
            <form className="add-category-form">
              <fieldset>
                <div className="form-group">
                  <div>
                    <label htmlFor="parentId">Category</label>
                    <Select id="parentId" name="parentId" defaultValue={this.props.parentId} style={{ width: '100%' }} onChange={this.handleCategoryChange}>
                    <Option key="0" value={0}>-- 1st Level Category --</Option>
                    {
                      categories.map(category => <Option key={category.id} value={category.id}>{category.name}</Option>)
                    }
                    </Select>
                  </div>
                  {
                    parentId > 0 ? 
                    <div>
                      <label htmlFor="subParentId">Sub Category</label>
                      <Select id="subParentId" defaultValue={this.props.subParentId} name="subParentId" style={{ width: '100%' }} onChange={this.handleSubCategoryChange}>
                      <Option key="0" value={0}>-- 2nd Level Category --</Option>
                      {
                        subCategories.map(category => <Option key={category.id} value={category.id}>{category.name}</Option>)
                      }
                      </Select>
                      
                    </div>
                    : null
                  }
                  <div>
                    <label htmlFor="categoryName">Category Name</label>
                    <Input id="categoryName" name="categoryName" className="category-input" 
                      onChange={this.handleChange} value={this.state.form.categoryName}/>
                    {this.state.form.formErrors.categoryName.length > 0  && (
                      <Alert message={this.state.form.formErrors.categoryName} type="error"/>
                    )} 
                  </div>
                </div>
                
              </fieldset>

            </form>
          </div>
          
        </Modal>
      </>
    );
  }
}