import React, { Component } from "react";
import {Link} from 'react-router-dom';
import './left-nav.css';
import { Layout, Menu, Breadcrumb } from 'antd';
import { UserOutlined, DesktopOutlined, LaptopOutlined, NotificationOutlined } from '@ant-design/icons';

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;

export default class LeftNav extends Component {
  render() {
    return (
      // <div className="left-nav"></div>
      <Menu
        mode="inline"
        defaultSelectedKeys={['item1']}
        defaultOpenKeys={['i1']}
        style={{ height: '100%', borderRight: 0 }}
        theme="dark"
      >
        <Menu.Item key="item1" icon={< DesktopOutlined />}>
          <Link to='/home'>Home</Link>
        </Menu.Item>
        <SubMenu key="sub2" icon={<LaptopOutlined />} title="Product">
          <Menu.Item key="1">
            <Link to='/category'>Product Category</Link>
          </Menu.Item>
          <Menu.Item key="2">
            <Link to='/product'>Product</Link>
          </Menu.Item>
        </SubMenu>
        <Menu.Item key="item3" icon={< UserOutlined />} >
          
          <Link to='/user'>User</Link>
        </Menu.Item>
        <Menu.Item key="item4" icon={< DesktopOutlined />}>
          <Link to='/role'>Role</Link>
        </Menu.Item>
      </Menu>
    )
  }
}