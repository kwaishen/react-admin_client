import React, { Component } from "react";
import {Link, withRouter  } from "react-router-dom";
import './header.css';
import { Avatar, Menu, Spin, Dropdown, message, Divider } from 'antd';
import { DownOutlined, LogoutOutlined, SettingOutlined, UserOutlined } from '@ant-design/icons';
import memoryUtils from "../../utils/memoryUtils";
import storageUtils from "../../utils/storageUtils";
import { logout } from "../../api/logout-service";

const onClick = ({ key }) => {
  switch (key) {
    // case 'logout':
    //   this.handleLogout;
    //   break;
    case 'profile' :
      message.info(`Click on item ${key}`);
      break;
    default:
      message.info(`Click on item ${key}`);
  }
};


const menu2 = (
  <Menu onClick={onClick}>
    <Menu.Item key="1">Sign in</Menu.Item>
    <Menu.Item key="2">Register</Menu.Item>
  </Menu>
);

class Header extends Component {

  handleLogout = async() => {
    // delete local storage
    storageUtils.deleteUser();
    // clear memory
    memoryUtils.user = {};
    // call server to remove session
    const res = await logout();
    if (res.code === 0) {
      // navigate to login page
      const { history } = this.props;
      if (history) {
        history.push('/login');
      }
    } else {
      message.error('Logout failed');
    }
    
  }

  render() {
    const user = memoryUtils.user;
    // user.avatar = 'https://iconscout.com/icon/avatar-370';
    const menu = (
      <Menu onClick={onClick}>
        <Menu.Item key="profile">
          <UserOutlined />
          My Profile
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="logout" onClick={this.handleLogout}>
          <LogoutOutlined />
            Logout
        </Menu.Item>
        {/* <Menu.Item key="3">3rd menu item</Menu.Item> */}
      </Menu>
    );

    return (
      <div className="global-header">
        <div style={{flex: '1 1 0%'}}></div>
        <div>
          <Dropdown overlay={menu }>
            <span  className="header-right">
              <span><Avatar className="avatar" size="small" icon={<UserOutlined />} ></Avatar></span>
              <span>{user.username}</span>
            </span>
        </Dropdown>
        </div>
      </div>
    )
  }
}

export default withRouter(Header);