import React, {Component} from 'react';
import { Redirect } from 'react-router-dom';
// import {Form, Icon, Input, Button} from  'antd';
import './login.css';

import { loginAsSeller } from '../../api/login-service';
import memoryUtils from '../../utils/memoryUtils';
import { message } from 'antd';
import storageUtils from '../../utils/storageUtils';

const emailRegex = RegExp(
  /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);

const formValid = ({ formErrors, ...rest }) => {
  let valid = true;

  // validate form errors being empty
  Object.values(formErrors).forEach(val => {
    val.length > 0 && (valid = false);
  });

  // validate the form was filled out
  Object.values(rest).forEach(val => {
    (val === null || val.length == 0) && (valid = false);
  });

  return valid;
};

export default class Login extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      email: 'henghui',
      password: 'henghui',
      formErrors: {
        email: '',
        password: ''
      }
    }
  }


  onChange = (e) => {
    e.preventDefault();
    const {name, value} = e.target;
    console.log(`name is ${name} and value is ${value}`)
    let formErrors = this.state.formErrors;

    switch (name) {
      case 'email':
        formErrors.email = 
          value.length < 3 && value.length > 0 ? "Minimum 3 characters required": "";
        break;
      case 'password':
        formErrors.password = 
          value.length < 6 && value.length > 0 ? "Minimum 6 characters required": "";
        break;
      default:
        break;
    }

    this.setState({[name]: value }, () => console.log(this.state))

    // this.setState({ formErrors }, () => console.log(this.state))
  }

  
  onSubmit = async(event) => {
    event.preventDefault();
    if (formValid(this.state)) {
      console.log(`Submitting username: ${this.state.email} password: ${this.state.password}`);
      const res = await loginAsSeller(this.state.email, this.state.password);
      if (res.code === 0) {
        console.log('login successful', res.data);    
        memoryUtils.user = res.data;
        storageUtils.saveUser(res.data);
        this.props.history.replace('/');
      } else {
        message.error('Login unsuccessful please try again.');
      }

    } else {
      // set errors
      console.error("FORM INVALID - DISPLAY ERROR MESSAGE");
    }

  }

  render() {
    const mystyle = {
        paddingTop: "20px",
        textAlign: "center"
    };

    const user = memoryUtils.user;
    if (user && user.id) {
      return <Redirect to='/'></Redirect>
    }

    return (
<div className="row login-container" style={mystyle}>
  <div className="col-sm-12" id="content">
    <div className="col-md-5 col-centered" style={{margin : "0 auto"}}>
      <div className="panel panel-default">
        <div className="panel-heading">
          <h3 className="panel-title">Login to React Admin Client</h3>
        </div>
        <div className="panel-body">
            <form onSubmit={this.onSubmit}>
              <fieldset>
                <div className="form-group login-input-item ">
                  <input type="text" name="email" value={this.state.email} onChange={this.onChange}
                   placeholder="Your email address" className={this.state.formErrors.email.length > 0 ? "error form-control" : "form-control"}  autoComplete="off"></input>
                  {this.state.formErrors.email.length > 0  && (
                    <div>
                      <p className="alert alert-danger">{this.state.formErrors.email}</p>
                    </div>
                  )} 
                  
                </div>
                <div className="form-group login-input-item">
                  <input type="password" name="password" value={this.state.password} onChange={this.onChange}
                   placeholder="Enter Password" className={this.state.formErrors.password.length > 0 ? "error form-control" : "form-control"} autoComplete="off"></input>
                  {this.state.formErrors.password.length > 0  && (
                    <div>
                      <p className="alert alert-danger">{this.state.formErrors.password}</p>
                    </div>
                  )} 
                </div>
              </fieldset>
              <fieldset>
                <button id="loginBtn" type="submit" className="btn btn-danger" >Log in</button>
              </fieldset>
            </form>

          <div className="login-register-container">
            <small>
              <span>Do not have an account? </span>
              <a className="text-danger" >Sign up!</a>
            </small>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
        )
    }
}