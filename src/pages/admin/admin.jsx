import React, {Component} from 'react';
import {Route, Redirect, Switch} from 'react-router-dom';
import memoryUtils from '../../utils/memoryUtils';
import { Layout } from 'antd';
import LeftNav from '../../components/left-nav/left-nav';
import Header from '../../components/header/header';
import Home from '../home/home';
import Product from '../product/product';
import Category from '../category/category';
import User from '../user/user';
import Role from '../role/role';
// import Breadcrumb from '../../components/breadcrumb/breadcrumb';


const {  Footer, Sider, Content } = Layout;

export default class Admin extends React.Component {
  
    render() {
      const user = memoryUtils.user;
      if (!user || !user.id) {
        return <Redirect to='login'/>
      }
        return (
<Layout style={{height:'100%'}}>
  <Sider width={200} className="site-layout-background">
    <LeftNav/>
  </Sider>
  <Layout>
    <Header></Header>
    <Content /*style={{backgroundColor: '#FFF'}}*/>
      {/* <Breadcrumb></Breadcrumb> */}
      {/* 2nd level routing */}
      <Switch>
        <Route path='/home' component={Home}></Route>
        <Route path='/user' component={User}></Route>
        <Route path='/product' component={Product}></Route>
        <Route path='/category' component={Category}></Route>
        <Route path='/role' component={Role}></Route>
        <Redirect to='/home' />
      </Switch>
    </Content>
    <Footer style={{textAlign : 'center'}}>Welcome to React Admin Client</Footer>
  </Layout>
</Layout> 

        )
    }
}