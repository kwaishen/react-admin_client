import React, { Component } from "react";
import { Modal, Button, message, Select, Input, Alert  } from 'antd';
import { createCategory, listCategoriesByParentId } from "../../api/category-service";


export class UpdateCategoryForm extends React.Component {

    render() {
    const {categoryName} = this.props.form;
    const {formErrors} = this.props.form;
    return (
      <div>
        <label htmlFor="categoryName">Category Name</label>
        <Input id="categoryName" name="categoryName" className="category-input" 
          onChange={this.props.onCategoryNameChange} value={categoryName}/>
        {formErrors.categoryName.length > 0  && (
          <Alert message={formErrors.categoryName} type="error"/>
        )} 
      </div>
    
    );
  }

  

}