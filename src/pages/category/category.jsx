import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import { Table, Tag, Space, message, Card, Button, Modal  } from 'antd';
import './index.css'
import { listAllCategory, listCategoriesByParentId, updateCategory } from '../../api/category-service';
import {CategoryDialog} from '../../components/dialog/category-dialog';
import { UpdateCategoryForm } from './update-category-form';

const { Column, ColumnGroup } = Table;


export default class Category extends Component {
  state = {
    title: 'Category',
    categories: [],
    subCategories: [],
    thirdCategories: [],
    loadling: false,
    parentId: 0,
    parentName: '',
    subParentId: 0,
    subParentName: '',
    level: 1,
    showStatus: 0, /* 0:hidden, 1:show add form, 2:show edit form*/
    editCategoryForm: {
      categoryId: 0,
      categoryName: '',
      formErrors: {
        categoryName: ''
      }
    }
  }

  initColumns = () => {
    this.columns = [
      {
          title: 'Id',
          dataIndex: 'id',
          key: 'id',
          width: '150px'
      },
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name' 
      },
      {
        title: 'Level',
        dataIndex: 'catLevel',
        key: 'catLevel',
        width: '150px'
      },
      {
        title: 'Action',
        key: 'action',
        width: '300px',
        render: (text, record) => (
          <Space size="middle">
            <Button type="link" onClick={() => this.showEditForm(record)}>Edit</Button>
            {record.catLevel > 2 ? null : 
            <Button type='link' onClick={() => this.handleShowSubCategories(record)}>Show Sub Categories</Button>  
            }
          </Space>
        ),
      },
    ]
  }

  /**
   * retrieve 1st/2nd/3rd level categories
   */
  getCategories = async () => {
    this.setState({loading: true})
    const {level, parentId, subParentId} = this.state;
    const pParam = (level < 3) ? parentId : subParentId;
    const res = await listCategoriesByParentId(pParam);
    this.setState({loading: false})
    
    if (res.code === 0) {
      // success
      const categories = res.data;
      if (this.state.level === 1) {
        this.setState({categories});
      } else if (this.state.level === 2) {
        this.setState({
          subCategories : categories
        });
      } else {
        this.setState({thirdCategories : categories});
      }
    } else {
      message.error('Unable to fetch categories')
    }
  }

  /**
   * Dispay edit category form
   */
  showEditForm = ({id, name}) => {
    this.setState({
      editCategoryForm: {
        categoryId: id,
        categoryName: name,
        formErrors: {
          categoryName: ''
        }
      }
    }, () => this.setState({showStatus: 2}))
    
  }

  /**
   * Hide dialog
   */
  handleCancel = () => {
    // reset fields
    // this.resetFields();
    this.setState({
      showStatus: 0
    });
  };

  handleCategoryNameChange = (e) => {
    e.preventDefault();
    const {name, value} = e.target;
    console.log(`name is ${name} and value is ${value}`)
    let formErrors = this.state.editCategoryForm.formErrors;
    formErrors.categoryName = !value || value.length < 1 ? "Category name is required": "";
    this.setState({
      editCategoryForm: {
        ...this.state.editCategoryForm, // keep everything else unchanged
        [name]: value
     }
    }, () => console.log(this.state.editCategoryForm))
  }

  /**
   * Call api to update category
   */
  handleUpdateCategory = async() => {
    const {categoryId, categoryName} = this.state.editCategoryForm;
    console.log(`updating category name ${categoryName}`)
    // ajax call to update category name
    const res = await updateCategory(categoryId, categoryName);
    if (res.code === 0) {
      this.setState({
        showStatus: 0,
      });
      message.success('Update category successfully')
      // refresh list
      this.getCategories();
    } else {
      message.error('Unable to update category')
    }
  }

  /**
   * Call back function after saving a category
   */
  handleSaveOk = () => {
    this.getCategories();
  }

  /**
   * Life cycle hook: run once
   */
  componentWillMount () {
    this.initColumns();
  }

  /**
   * Life cycle hook: 
   */
  componentDidMount() {
    this.getCategories();
    
  }

  handleShowSubCategories = (row) => {
    // update name
    if (row.catLevel === 1) {
      this.setState({
        parentId: row.id,
        parentName: row.name,
        level: row.catLevel + 1
      }, () => {
        console.log('NEW LEVEL: ' + this.state.level)
        this.getCategories();
      })

    } else if (row.catLevel === 2) {
      this.setState({
        subParentId: row.id,
        subParentName: row.name,
        level: row.catLevel + 1
      }, () => {
        console.log('NEW LEVEL: ' + this.state.level)
        this.getCategories();
      })
    } 
  }



  render() {

    const {categories, loading, parentName, subParentName, title, level, parentId, subParentId, subCategories, thirdCategories} = this.state;
    console.log('parentId is: ' + parentId + ' and sub parentId is: ' + subParentId);
    console.log('parentName is: ' + parentName + ' and sub parentName is: ' + subParentName);
    
    return(
      <div>
        <Card className="category-table" title={(level === 1) ? title : (level === 2) ? title + ' -> ' + parentName : title + ' -> ' + parentName + ' -> ' + subParentName} 
          extra={<CategoryDialog categories={categories} subCategories={subCategories} parentId={parentId} subParentId={subParentId}
                  onSaveOk={this.handleSaveOk}
                ></CategoryDialog>} >
          
          <button type="button" className="ant-btn ant-btn-dashed" ant-click-animating-without-extra-node="false" style={{width: '100%', marginBottom: '8px'}}><span role="img" aria-label="plus" className="anticon anticon-plus">
            <svg viewBox="64 64 896 896" focusable="false" className="" data-icon="plus" width="1em" height="1em" fill="currentColor" aria-hidden="true"><defs><style></style></defs><path d="M482 152h60q8 0 8 8v704q0 8-8 8h-60q-8 0-8-8V160q0-8 8-8z"></path><path d="M176 474h672q8 0 8 8v60q0 8-8 8H176q-8 0-8-8v-60q0-8 8-8z"></path></svg></span><span>ADD</span></button>

          <Table dataSource={level === 1? categories : level === 2? subCategories : thirdCategories} 
            columns={this.columns}  
            rowKey='id'
            loading={loading}
            >
          </Table>

          <Modal
            title="Edit Category"
            visible={this.state.showStatus === 2}
            onOk={this.handleUpdateCategory}
            onCancel={this.handleCancel}
          >
            <UpdateCategoryForm 
              form={this.state.editCategoryForm}
              onCategoryNameChange={this.handleCategoryNameChange}
            
            ></UpdateCategoryForm>

          </Modal>

        </Card>
      

      </div>

    )
  }
}