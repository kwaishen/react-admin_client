import React, {Component} from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import ProductHome from './product-home';
import ProductAddUpdate from './product-add-update';

export default class Home extends Component {
  render() {
    return(
      <Switch>
        <Route path='/product' component={ProductHome} exact/>
        <Route path='/product/new' component={ProductAddUpdate}/>
        <Route path='/product/:productId' component={ProductAddUpdate} exact/>
        {/* <Route path='/product/detail' component={ProductHome}/>
        <Route path='/product/update' component={ProductHome}/> */}
        
        <Redirect to='/product' />
      </Switch>
    )
  }
}