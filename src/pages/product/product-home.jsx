import React, {Component} from 'react';
import { Table,Input,Radio, Avatar, List, Space, message, Card, Button, Modal  } from 'antd';
import './product.css'
import { getAllProducts } from '../../api/product-service';

const { Search } = Input;

// const data = [
//   {
//     id:1,
//     name: 'Computer',
//     description: 'This is a computer',
//     price: 1500.59,
//     status: 1,
//     stockNumber: 10

//   },
//   {
//     id:2,
//     name: 'Ultra Boost',
//     description: 'This is Adidas Shoes',
//     price: 199.99,
//     status: 2,
//     stockNumber: 0
    
//   },
//   {
//     id:3,
//     name: 'iPhone 11',
//     description: 'This is the lasted cellphone from Apple',
//     price: 1099.99,
//     status: 3,
//     stockNumber: 0
    
//   },
//   {
//     id:4,
//     name: 'Harry Potter The Order of Phoneix',
//     description: 'From Jk Rolling',
//     price: 19.99,
//     status: 0,
//     stockNumber: 0
//   }
// ]

const options = [
  { label: 'In Stock', value: 'inStock' },
  { label: 'Out of Stock', value: 'outOfStock' },
  { label: 'Discontinued', value: 'discontinued' },
];

export default class ProductHome extends Component {
  state = {
    stockStatus: 0,
    products: []
  }

  initColumns = () => {
    this.columns = [
      // {
      //   title: 'Id',
      //   dataIndex: 'id',
      //   key: 'id',
      //   width: '150px'
      // },
      // {
      //   title: 'Name',
      //   dataIndex: 'name',
      //   key: 'name' 
      // },
      // {
      //   title: 'Description',
      //   dataIndex: 'description',
      //   key: 'description',
      //   width: '150px'
      // },
      {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
        width: '100px'
      },
      {
        title: 'Status',
        dataIndex: 'status',
        key: 'status',
        width: '100px'
      },
      {
        title: 'Stock Number',
        dataIndex: 'stock',
        key: 'stock',
        width: '100px'
      /*},
      {
        title: 'Action',
        key: 'action',
        width: '300px',
        render: (text, record) => (
          <Space size="middle">
            <Button type="link" onClick={() => this.showEditForm(record)}>Edit</Button>
            <Button type="link" onClick={() => this.handleDelete(record)}>Edit</Button>
          </Space>
        ),*/
      }
    ]
  }

  getProducts = async() => {
    this.setState({loading: true});
    const res = await getAllProducts();
    this.setState({loading: false});
    if (res.code === 0) {
      const products = res.data;
      this.setState({products: products});
    } else {
      message.error('Unable to get products')
    }
    
  }

  displayStatus = ({status}) => {
    switch (status) {
      case 0:
        return <p className="in-stock">In Stock</p>
        break;
      case 1:
        return <p className="out-of-stock">Out of Stock</p>
        break;
      case 2:
        return <p className="discontinued">Discontinued</p>
        break;
      default:
        return <p className="status-na">N/A</p>
    }
  }

  // displayColumns = (items) => {
  //   let rows = [];
  //   items.forEach(item => {
  //     rows.push(
  //       <div className="list-col">
  //         <span>item.label</span>
  //         <p>{this.displayStatus(item)}</p>
  //       </div>
  //     )
  //   });
  //   return rows;
  // }

  handleEditProduct = (item) => {
    this.props.history.push('/product/' + item.id, item);
    
  }

  handleDelete = (item) => {
    
  }

  handleStatusChange = (e) => {
    // const {label, value} = status;
    // console.log(`change to ${label}`);
    this.setState({
      stockStatus: e.target.value,
    });
  }

  handleNewProduct = (e) => {
    console.log(`create new product`)
    this.props.history.push('/product/new')
  }

  
  /**
   * Life cycle hook: run once
   */
  componentWillMount () {
    this.initColumns();
  }

  /**
   * Life cycle hook: 
   */
  componentDidMount() {
    this.getProducts();
    
  }

  render() {
    const {products} = this.state;
    const {stockStatus} = this.state;
    const searchBar = (
      <div className="search-bar">
        <Radio.Group
          className="search-bar-widget"
          options={options}
          onChange={this.handleStatusChange}
          value={stockStatus}
          optionType="button"
          
        />
        <Search
          className="search-bar-widget"
          placeholder="input search text"
          onSearch={value => console.log(value)}
          style={{ width: 200 }}
      />
      </div>
    )

    return(
      <div>
        <Card className="product-list" title="Product" extra={searchBar}>
          <button type="button" className="ant-btn ant-btn-dashed" ant-click-animating-without-extra-node="false" 
            style={{width: '100%', marginBottom: '8px'}} onClick={this.handleNewProduct}><span role="img" aria-label="plus" className="anticon anticon-plus"
            >
            <svg viewBox="64 64 896 896" focusable="false" className="" data-icon="plus" width="1em" height="1em" fill="currentColor" aria-hidden="true"><defs><style></style></defs><path d="M482 152h60q8 0 8 8v704q0 8-8 8h-60q-8 0-8-8V160q0-8 8-8z"></path><path d="M176 474h672q8 0 8 8v60q0 8-8 8H176q-8 0-8-8v-60q0-8 8-8z"></path></svg>
              </span><span>ADD</span>
            </button>

          <List
            itemLayout="horizontal"
            dataSource={products}
            renderItem={item => (
              <List.Item>
                <List.Item.Meta
                  avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                  title={<a href="https://ant.design">{item.name}</a>}
                  description={item.description}
                />

    
                <div className="list-col">
                  <span>Price</span>
                  <p>${item.price}</p>
                </div>
                
                <div className="list-col">
                  <span>Status</span>
                  {this.displayStatus(item)}
                </div>
                <div className="list-col">
                  <span>Stock Number</span>
                  <p>{item.stockNumber}</p>
                </div>
                <div className="list-col">
                  <Space size="middle">
                    <Button type="link" onClick={() => this.handleEditProduct(item)}>Edit</Button>
                    <Button type='link' onClick={() => this.handleDelete(item)}>Delete</Button>  
                  </Space>

                </div>
          </List.Item>
            )}
          />
        </Card>
      </div>
      
    )
  }
}