import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import { Table,Input,Radio, Select, Alert, Avatar, List, Space, message, Card, Button, Breadcrumb  } from 'antd';
import { ArrowLeftOutlined, ArrowRightOutlined} from '@ant-design/icons';
import './product.css'
import { createProduct } from '../../api/product-service';
import { listCategoriesByParentId } from '../../api/category-service';

const { TextArea } = Input;
const { Option } = Select;


export default class ProductAddUpdate extends Component {
  constructor(props) {
    super(props);

    let product = this.props.history.location.state;
    let isUpdate = true;
    if (!product || product === {} || product === undefined) {
      isUpdate = false;
    }

    this.state = {
      categories: [],
      subCategories: [],
      thirdCategories: [],
      form: {
        name: isUpdate ? product.name : '',
        description: isUpdate ? product.description : '',
        price: isUpdate ? product.price : 0,
        status: isUpdate ? product.status : 0,
        categoryId: isUpdate ? product.productCategoryId : 0,
        subCategoryId: isUpdate ? product.subProductCategoryId : 0,
        thirdCategoryId: isUpdate ? product.thirdProductCategoryId: 0,
        stockNumber: isUpdate ? product.stockNumber : 0,
        formErrors: {
          name: '',
          description: '',
          price: '',
          status: '',
          categoryId: '',
          subCategoryId: '',
          thirdCategoryId: '',
          stockNumber: ''
        }
      }
    }
  }


  /**
   * Life cycle hook: run once
   */
  componentDidMount() {

    this.loadCategories(1);
    const {subCategoryId, thirdCategoryId} = this.state.form;
    if (subCategoryId > 0) {
      this.loadCategories(2);
    }
    if (thirdCategoryId > 0) {
      this.loadCategories(3);
    }
     console.log(this.state.form);
  }


  /**
   * Life cycle hook: run twice
   */
  componentWillMount () {

  }

  loadCategories = async(level) => {
    let categoryId = 0;
    if (level === 2) {
      categoryId = this.state.form.categoryId;
    } else if (level === 3) {
      categoryId = this.state.form.subCategoryId;
    } 

    const res = await listCategoriesByParentId(categoryId);
    if (res.code === 0) {
      // success
      const categories = res.data;
      this.setState(this.setCategories(level, categories), () => console.log(this.state.form))
    } else {
      message.error('Unable to fetch categories')
    }
  }

  /**
   * Update specific category array by level
   */
  setCategories = (level, data) => {
    if (level === 1) {
      return {categories: data};
    } else if (level === 2) {
      return {subCategories: data}
    } else if (level === 3) {
      return {thirdCategories: data}
    }
  }

  validate = (name, value) => {
    const {formErrors} = this.state.form;
    switch (name) {
      case 'name':
        formErrors.name = 
          !value || value.length < 1 ? "Category name is required": "";
        break;
      case 'description':
        formErrors.description = 
          !value || value.length < 1 ? "Description is required": "";
        break;
      case 'price':
        // const { value } = e.target;
        // const reg = /^-?\d*(\.\d*)?$/;
        // if ((!isNaN(value) && reg.test(value)) || value === '' || value === '-') {
        //   this.props.onChange(value);
        // }
        formErrors.price = 
          !value || value.length < 1 ? "Price is required": "";
        break;
      case 'categoryId':
        formErrors.categoryId = 
          !value || value === 0 ? "Category is required": "";
      case 'status':
        formErrors.stockNumber = 
          !value || value.length < 1 ? "Status is required": "";          
        break;          
      case 'stockNumber':
        formErrors.stockNumber = 
          !value || value.length < 1 ? "Stock number is required": "";
        break;          
      default:
        break;
    }

    this.setState({
      form: {
        ...this.state.form, // keep everything else unchanged,
        formErrors: formErrors,
     }
    }, /*() => console.log(this.state.form)*/)
  }
  

  /**
   * Handle form field change
   */
  handleChange = (e) => {
    e.preventDefault();
    const {name, value} = e.target;
    console.log(`name is ${name} and value is ${value}`)

    // validate
    this.validate(name, value);

    // setting value
    this.setState({
      form: {
        ...this.state.form, // keep everything else unchanged,
        [name]: value
     }
    }, () => console.log(this.state.form))
  }

  /**
   * Handle 1st level category change
   */
  handleCategoryChange = (val) => {
    console.log(`category selected ${val}`);
    this.setState({
      form: {
        ...this.state.form, // keep everything else unchanged
        categoryId: val,
        subCategoryId: 0,
        thirdCategoryId: 0,
        subCategories: [],
        thirdCategories: [],
        formErrors:  {
          ...this.state.form.formErrors,
          categoryId: val > 0 ? '' : 'Category is required'
        }
     }
    }, () => {
      console.log(this.state.form);
      this.loadCategories(2);
    })
  }

  handleSubCategoryChange = (val) => {
    console.log(`sub category selected ${val}`);
    this.setState({
      form: {
        ...this.state.form, // keep everything else unchanged
        subCategoryId: val,
        thirdCategoryId: 0,
        thirdCategories: []
     }
    }, () => {
      console.log(this.state.form);
      this.loadCategories(3);
    })
  }

  handleThirdCategoryChange = (val) => {
    console.log(`sub category selected ${val}`);
    this.setState({
      form: {
        ...this.state.form, // keep everything else unchanged
        thirdCategoryId: val
     }
    }, () => {
      console.log(this.state.form);
    })
  }


  handleStatusChange = (status) => {
    this.setState({
      form: {
        ...this.state.form, // keep everything else unchanged
        status: status
     }
    }, () => console.log(this.state.form))
  }


  /**
   * Submit Form --> Invoke API
   */
  handleSumbit = async(form) => {
    const {name, description, price, status, stockNumber, categoryId, subCategoryId, thirdCategoryId, formErrors} = this.state.form;
    let product = {
      name: name,
      description: description,
      status: status,
      price: Number(price),
      stockNumber: stockNumber
    };    

    // set categoryId
    if (categoryId > 0 && subCategoryId > 0 && thirdCategoryId > 0) {
      product.categoryId = thirdCategoryId;
    } else if (categoryId > 0 && subCategoryId > 0) {
      product.categoryId = subCategoryId
    } else {
      product.categoryId = categoryId;
    }

    // validate form
    Object.keys(product).forEach(key => {
      this.validate(key, product[key])
    })

      // validate form errors being empty
    let valid = true;
    Object.values(formErrors).forEach(val => {
      val.length > 0 && (valid = false);
    });

    if (valid) {
      const res = await createProduct(product);
      if (res.code === 0) {
        message.success('Product created successfully');
        this.props.history.push('/product');
      } else {
        message.error('Unable to create product');
      }
    } else {
      message.error('FORM INVALID - DISPLAY ERROR MESSAGE')
    }

  }

  handleCancel = () => {
    this.props.history.push('/product');
  }

  render() {
    const {productId} = this.props.match.params;
    const {categories, subCategories, thirdCategories} = this.state;
    const {name, categoryId, subCategoryId, thirdCategoryId, description, price, status, stockNumber, formErrors} = this.state.form;
    
    const title = (
      <Breadcrumb separator=">" style={{fontSize: '16px'}}>
        <Breadcrumb.Item id="product">
          <Link to='/product' type="button">Product</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item id="addUpdateProduct">{productId ? 'Update product' : 'Add Product'}</Breadcrumb.Item>
      </Breadcrumb>
    )
    
    return(
      <div>
        <Card className="product-list" title={title} >
          <div className="add-update-form"> 
            <form action="">
              <fieldset>
                <div className="product-row">
                  <label htmlFor="productName">Product Name</label>
                  <Input id="name" name="name" className="category-input" 
                    onChange={this.handleChange} value={name}/>
                  {formErrors.name.length > 0  && (
                    <Alert message={formErrors.name} type="error"/>
                  )} 
                </div>
                <div className="product-row">
                  <label htmlFor="description">Description</label>
                  <TextArea rows={4} id="description" name="description" className="category-input" 
                    onChange={this.handleChange} value={description}/>
                  {formErrors.description.length > 0  && (
                    <Alert message={formErrors.description} type="error"/>
                  )} 
                </div>

                <div className="product-row">
                  <label htmlFor="categoryId">Prodcut Category</label>
                  <Select id="categoryId" name="categoryId" defaultValue={categoryId} style={{ width: '100%' }} onChange={this.handleCategoryChange}>
                    <Option key="0" value={0}>-- 1st Level Category --</Option>
                    {
                      categories.map(category => <Option key={category.id} value={category.id}>{category.name}</Option>)
                    }
                  </Select>
                  {formErrors.categoryId.length > 0  && (
                    <Alert message={formErrors.categoryId} type="error"/>
                  )} 

                </div>   
                {
                  categoryId > 0 ? 
                  <div className="product-row"> 
                    <label htmlFor="subCategoryId">Sub Category</label>
                    <Select id="subCategoryId" value={subCategoryId} defaultValue={subCategoryId} name="subCategoryId" style={{ width: '100%' }} onChange={this.handleSubCategoryChange}>
                    <Option key="0" value={0}>-- 2nd Level Category --</Option>
                    {
                      subCategories.map(category => <Option key={category.id} value={category.id}>{category.name}</Option>)
                    }
                    </Select>
                    
                  </div>
                  : null
                }
                {
                  (subCategoryId > 0 && categoryId > 0) ? 
                  <div className="product-row"> 
                    <label htmlFor="thirdCategoryId">Third Level Category</label>
                    <Select id="thirdCategoryId" value={thirdCategoryId} defaultValue={thirdCategoryId} name="thirdCategoryId" style={{ width: '100%' }} onChange={this.handleThirdCategoryChange}>
                    <Option key="0" value={0}>-- 3rd Level Category --</Option>
                    {
                      thirdCategories.map(category => <Option key={category.id} value={category.id}>{category.name}</Option>)
                    }
                    </Select>
                    
                  </div>
                  : null
                }

                <div className="product-row">
                  <label htmlFor="status">Product Status</label>
                  <Select id="status" name="status" defaultValue={status} style={{ width: '100%' }} onChange={this.handleStatusChange}>
                    <Option key="0" value={0}>In Stock</Option>
                    <Option key="1" value={1}>Out of Stock</Option>
                  {/* {
                    categories.map(category => <Option key={category.id} value={category.id}>{category.name}</Option>)
                  } */}
                  </Select>
                </div>
                <div className="product-row">
                  <label htmlFor="price">Price</label>
                  <Input id="price" name="price" className="category-input" prefix="$" suffix="CAD"
                    onChange={this.handleChange} value={price}/>
                  {formErrors.price.length > 0  && (
                    <Alert message={formErrors.price} type="error"/>
                  )} 
                </div>
                <div className="product-row">
                  <label htmlFor="stockNumber">Stock Number</label>
                  <Input id="stockNumber" name="stockNumber" className="category-input"
                    onChange={this.handleChange} value={stockNumber}/>
                  {formErrors.stockNumber.length > 0  && (
                    <Alert message={formErrors.stockNumber} type="error"/>
                  )} 
                </div>

              </fieldset>

              <fieldset className="submit-row"> 
                <Button type="primary" onClick={this.handleSumbit}>Submit</Button>
                <Button onClick={this.handleCancel}>Cancel</Button>
              </fieldset>
            </form>
          </div>

        </Card>
          
      </div>
    )
  }
}