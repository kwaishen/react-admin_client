import ajax from "./ajax";

const BASE_URL = '/myProduct/api/productcategory';

export const listAllCategory = () => {
    return ajax(BASE_URL + '/all', {}, 'GET');
}

export const listCategoriesByParentId = (parentId) => {
  return ajax(BASE_URL + '/list', {parentId}, 'GET');
}

export const createCategory = (name, parentCid) => {
  return ajax(BASE_URL , {name, parentCid}, 'POST');
}

export const updateCategory = (categoryId, categoryName) => {
  return ajax(BASE_URL + '/' + categoryId , {categoryId, categoryName}, 'PUT');
}
