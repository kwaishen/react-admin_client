import ajax from "./ajax";

const BASE_URL = '/myProduct/api/product';

export const getAllProducts = () => {
    return ajax(BASE_URL + '/all', {}, 'GET');
}

export const createProduct = (product) => {
  return ajax(BASE_URL, product, 'POST');

}
