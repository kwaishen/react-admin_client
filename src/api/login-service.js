import ajax from "./ajax";

// const BASE_URL = 'http://localhost:8083/api/login';
const BASE_URL = '/myUser/api/login';

export function loginAsBuyer(openId) {
    ajax(BASE_URL + '/buyer', {'openId': openId}, 'POST');
}

export const loginAsSeller = (username, password) => {
    // return ajax(BASE_URL + '/seller', {'username': username, 'password' : password }, 'POST');
    return ajax(BASE_URL + '/seller', {username, password }, 'POST');
    // ajax('http://localhost:8083/api/login/seller', {username, password }, 'POST');
}

