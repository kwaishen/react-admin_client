/*
能发送异步ajax请求的函数模块
封装axios库
函数的返回值是promise对象
*/
import axios from 'axios';
import { message } from 'antd';

export default function ajax(url, data={}, type='GET') {

  return new Promise((resolve, reject) => {
    let promise;
    // 1. execute async ajax request
    if (type ==='GET') {
      promise = axios.get(url, {params: data});
    } else if (type ==='POST') {
      promise = axios.post(url, data);
    } else if (type === 'PUT') {
      promise = axios.put(url, data);
    }
       
    // 2. if request is successful, invoke resolve()
    promise.then(res => {
      resolve(res.data)
    // 3. if request failed, do NOT invoke reject
    }).catch(error => {
      message.error('Request failed: ' + error.message)
    })
    
  });

}