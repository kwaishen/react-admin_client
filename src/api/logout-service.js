import ajax from "./ajax";

const BASE_URL = '/myUser/api/logout';

export const logout = () => {
  return ajax(BASE_URL + '/', {}, 'GET');
}
